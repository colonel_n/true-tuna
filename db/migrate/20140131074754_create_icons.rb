class CreateIcons < ActiveRecord::Migration
  def change
    create_table :icons do |t|
      t.integer :user_id
      t.string :path

      t.timestamps
    end
  end
end
