require 'test_helper'

class PersonalIconsControllerTest < ActionController::TestCase
  setup do
    @personal_icon = personal_icons(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:personal_icons)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create personal_icon" do
    assert_difference('PersonalIcon.count') do
      post :create, personal_icon: { path: @personal_icon.path, user_id: @personal_icon.user_id }
    end

    assert_redirected_to personal_icon_path(assigns(:personal_icon))
  end

  test "should show personal_icon" do
    get :show, id: @personal_icon
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @personal_icon
    assert_response :success
  end

  test "should update personal_icon" do
    patch :update, id: @personal_icon, personal_icon: { path: @personal_icon.path, user_id: @personal_icon.user_id }
    assert_redirected_to personal_icon_path(assigns(:personal_icon))
  end

  test "should destroy personal_icon" do
    assert_difference('PersonalIcon.count', -1) do
      delete :destroy, id: @personal_icon
    end

    assert_redirected_to personal_icons_path
  end
end
