require 'spec_helper'

describe "icons/index" do
  before(:each) do
    assign(:icons, [
      stub_model(Icon,
        :user_id => 1,
        :path => "Path"
      ),
      stub_model(Icon,
        :user_id => 1,
        :path => "Path"
      )
    ])
  end

  it "renders a list of icons" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Path".to_s, :count => 2
  end
end
