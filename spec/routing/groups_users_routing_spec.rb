require "spec_helper"

describe GroupsUsersController do
  describe "routing" do

    it "routes to #index" do
      get("/groups_users").should route_to("groups_users#index")
    end

    it "routes to #new" do
      get("/groups_users/new").should route_to("groups_users#new")
    end

    it "routes to #show" do
      get("/groups_users/1").should route_to("groups_users#show", :id => "1")
    end

    it "routes to #edit" do
      get("/groups_users/1/edit").should route_to("groups_users#edit", :id => "1")
    end

    it "routes to #create" do
      post("/groups_users").should route_to("groups_users#create")
    end

    it "routes to #update" do
      put("/groups_users/1").should route_to("groups_users#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/groups_users/1").should route_to("groups_users#destroy", :id => "1")
    end

  end
end
