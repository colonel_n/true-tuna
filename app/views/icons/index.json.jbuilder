json.array!(@icons) do |icon|
  json.extract! icon, :id, :user_id, :path
  json.url icon_url(icon, format: :json)
end
