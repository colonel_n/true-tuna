class Article < ActiveRecord::Base
  has_one :message
  has_one :photo
  has_many :favorites
end
