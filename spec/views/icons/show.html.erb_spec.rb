require 'spec_helper'

describe "icons/show" do
  before(:each) do
    @icon = assign(:icon, stub_model(Icon,
      :user_id => 1,
      :path => "Path"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Path/)
  end
end
