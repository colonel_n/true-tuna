# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# ユーザー
User.create id: 1, password: 'agasa', name: '阿笠 博士', email: 'm-naraoka@cnt.biglobe.co.jp'
User.create id: 2, password: 'konan', name: '江戸川 コナン', email: 'megane-alert@yuy.hq.biglobe.nec.co.jp'
User.create id: 3, password: 'hattori', name: '服部 平次', email: 'megane-rails@yuy.hq.biglobe.nec.co.jp'

# グループ
Group.create id: 1, name: 'システム管理者'
Group.create id: 2, name: '真実はいつも一つ'
Group.create id: 3, name: 'メガネども'

# 中間テーブル
GroupsUser.create id: 1, user_id: 1, group_id: 1
GroupsUser.create id: 2, user_id: 2, group_id: 1
GroupsUser.create id: 3, user_id: 3, group_id: 1

# 記事
Article.create id: 1, user_id: 1, group_id: 1
Article.create id: 2, user_id: 3, group_id: 1
Article.create id: 3, user_id: 2, group_id: 1

# メッセージ
Message.create id: 1, article_id: "1", text: "ワシじゃよ"
Message.create id: 2, article_id: "2", text: "バーロー"
Message.create id: 3, article_id: "3", text: "せやかて工藤"

# アイコン
Icon.create id: 1, user_id: 1, path: "agasa.jpg"
Icon.create id: 2, user_id: 2, path: "konan.jpg"
Icon.create id: 3, user_id: 3, path: "hattori.jpg"

# 写真
Photo.create id: 1, article_id: 2, path: "hattori.jpg"

# お気に入り
Favorite.create id: 1, article_id: 1, user_id: 1
