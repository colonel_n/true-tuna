require 'spec_helper'

describe "icons/edit" do
  before(:each) do
    @icon = assign(:icon, stub_model(Icon,
      :user_id => 1,
      :path => "MyString"
    ))
  end

  it "renders the edit icon form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", icon_path(@icon), "post" do
      assert_select "input#icon_user_id[name=?]", "icon[user_id]"
      assert_select "input#icon_path[name=?]", "icon[path]"
    end
  end
end
